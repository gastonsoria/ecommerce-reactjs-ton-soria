import NavBar from './components/navbar/NavBar';
import ItemListContainer from './container/itemlistcontainer/ItemListContainer';
import ItemDetailContainer from './container/itemdetailcontainer/ItemDetailContainer'
import Cart from "./components/cart/Cart"
import {BrowserRouter, Routes, Route, Navigate} from 'react-router-dom'
import CartContextProvider from './context/CartContext';


function App() {
  return (
   <BrowserRouter>
  <>
  <CartContextProvider> 
    <NavBar/>
      <Routes>

        <Route path="/" element={<ItemListContainer/>}/>
        <Route path="/categoria/:id" element={<ItemListContainer/>}/>
        <Route path="/detalle/:detalleId" element={<ItemDetailContainer/>}/>
        <Route path="/cart" element={<Cart/>}/>

        {/* NAVIGATE */}
        <Route path='/*' element={<Navigate to={'/'} replace/>}/>
      </Routes>
  </CartContextProvider>
  </>
  </BrowserRouter>

  )
}

export default App;
