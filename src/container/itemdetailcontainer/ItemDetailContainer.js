import {useState, useEffect} from 'react'
import { useParams } from 'react-router-dom'
import ItemDetail from '../../components/itemdetail/ItemDetail'
import { getFetch } from "../../helpers/getFech"



const ItemDetailContanier = () => {
    const [producto, setProducto] = useState({})
    const { detalleId } = useParams()
    const [loading, setLoading] = useState(true)


    useEffect(() => {
     getFetch(detalleId)
    .then(respuesta => setProducto(respuesta))
    .catch((err) => console.log(err) )
    .finally(()=>setLoading(false))
    }, [detalleId])

return (
    
    <>
    { loading ? <h2>Cargando...</h2> :
    
    <ItemDetail producto={producto}/>
    }
    </>
)
}

export default ItemDetailContanier