import { useEffect, useState } from "react"
import { useParams } from 'react-router-dom'
import ItemList from '../../components/itemlist/ItemList'
import { getFetch } from "../../helpers/getFech"





const ItemListContainer = ({ greeting = "hola! soy itemlistcontainer" }) => {
  const [productos, setProductos] = useState([])
  const [cargando, setCargando] = useState(true)
  const { id } = useParams()

  useEffect(() => {
    if (id) {
    getFetch()
    .then(respuesta=> setProductos(respuesta.filter((prods) => prods.categoria === id)))
    .catch((err)=> console.log(err))
    .finally(()=>setCargando(false))   

  }else{ 
    getFetch()
    .then(respuesta=> setProductos(respuesta))
            .catch((err)=> console.log(err))
            .finally(()=>setCargando(false))                 
        }
    }, [id])




  return (

    <>
      <h2>{greeting}</h2>

      
      {cargando ? <h3>Cargando...</h3> : <ItemList productos={productos} />} 
    </>
  )
}

export default ItemListContainer