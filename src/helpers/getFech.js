const productos = [
    
    {
        id:"1", 
        categoria:"libreria", 
        title:"lapiz plantable", 
        price:500, 
        pictureUrl:"/images/lapiz-plantable.jpg",
        detail:"lapices con semillas de zanahoria, lino, oregano, perejil y más" },
    {
        id:"2", 
        categoria:"organizadores", 
        title:"set organizador", 
        price:1000, 
        pictureUrl:"/images/set-organizador.jpg",
        detail:"Para mantener tu espacio de trabajo ordenado" },
    {
        id:"3", 
        categoria:"soportes", 
        title:"soporte ipad", 
        price:1500, 
        pictureUrl:"/images/soporte-ipad.jpg",
        detail:"soporte para tablets y ipads" },
    {
        id:"4", 
        categoria:"soportes", 
        title:"soporte laptop", 
        price:2000, 
        pictureUrl:"/images/soporte-laptop.jpg",
        detail:"soporte para laptop con aeroventilas" }
  

]

export const getFetch = (id) => {
    return new Promise((resolve) => {
        setTimeout(()=>{ 
            const query = id ? productos.find(productos => productos.id === id ) : productos
            resolve( query )
        }, 2000 )

    })
}

