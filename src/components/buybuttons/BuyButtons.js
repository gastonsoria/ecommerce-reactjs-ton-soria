import { Link } from "react-router-dom";


export default function BuyButtons() {

    return (
        <div className="buyButtons">
            <Link to='/cart'>
                <button >Ir al Carrito</button>
            </Link>
            <Link to='/'>
                <button >Volver al menú</button>
            </Link>
        </div>
    )

}
