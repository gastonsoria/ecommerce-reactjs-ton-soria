import React from 'react'
import CartWidget from '../cartwidget/CartWidget'
import { Link } from 'react-router-dom'
import './../navbar/navbar.css'
import { useCartContext } from '../../context/CartContext'

const array = [
  { idCategoria: '1', name: 'libreria', nameButton: 'Libreria' },
  { idCategoria: '2', name: 'soportes', nameButton: 'Soportes' },
  { idCategoria: '3', name: 'organizadores', nameButton: 'Organizadores' },
]

const NavBar = () => {
  const {cantidadTotal} = useCartContext()
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">

          {/*CARTWIDGET*/}
          {cantidadTotal() !== 0 && cantidadTotal()}
          <CartWidget />
         

          {/* BRAND */}

          <Link to='/'><h1>Ton Soria</h1></Link>

          {/* BOTON */}
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          
          {/* SECCIONES */}
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">

              {array.map(param =>
                <Link to={`/categoria/${param.name}`} key={param.id}>
                  <li>
                    <a className="nav-link secciones" >
                      {param.nameButton}
                    </a>
                  </li>
                </Link>)}



            </ul>
          </div>
        </div>
        

      </nav>

      

 
    </>
  )
}

export default NavBar
