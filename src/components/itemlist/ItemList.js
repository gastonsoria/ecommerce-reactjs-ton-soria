import React from 'react'
import Item from '../item/Item'

function ItemList({productos}) {
  return ( 
        
        <div>
            {productos.map(merch => <Item key={merch.id} item={merch}/> )}
         </div>
  )
}

export default ItemList