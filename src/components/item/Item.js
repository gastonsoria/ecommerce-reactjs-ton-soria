import React from 'react'
import {Link} from 'react-router-dom'


function Item({item}) {

    return (

        
            
                <div key={item.id}>
                    <img src={item.pictureUrl}  alt={item.title} />
                    <div >
                        <h3>{item.title}</h3>
                        <p> ${item.price}</p>
                        <Link to={`/detalle/${item.id}`}><button>VER DETALLE modificar</button> </Link>
                    </div>
                </div>
            
        
    

    
    
    )
  
}

export default Item