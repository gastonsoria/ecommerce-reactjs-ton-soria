import {Link} from 'react-router-dom'
import ItemCount from '../itemcount/ItemCount'
import {useState} from 'react'
import BuyButtons from '../buybuttons/BuyButtons'
import { useCartContext } from '../../context/CartContext'

const ItemDetail = ({producto}) => {
  const [inputType, setInputType] = useState('itemCount');
  const {addToCart} = useCartContext()
 
  
  const onAdd = (count) => {
    addToCart( { ...producto, cantidad: count })
  }

 
  
  function handleInputType() {
    setInputType('buyButtons');
  }

  return (
    <>
  <Link to="/detalles/:detalleId"/>
    <div className="card">
      <img src={producto.pictureUrl} className="card-img-top" alt={producto.title}/>
      <div className="card-body">
        <h4 className="card-title">{producto.categoria}</h4>
        <h5 className="card-title">{producto.title}</h5>
        <p className="card-text">{producto.producto}</p>
        <p className="card-text">${producto.price}</p>
        <p>{producto.detail}</p>

        {inputType === 'itemCount' ?
                    <ItemCount producto={producto} initial={1} stock={5} onAdd={onAdd} handleInputType={handleInputType}/>:
                    <BuyButtons/>}
    
      </div>
    </div>
    
   
    </>
    
  ) 
}

export default ItemDetail