import React, {useState} from 'react'

import './itemcount.css'

function ItemCount({stock, initial, onAdd, producto, handleInputType}) {
    
    const [count, setCount] = useState(initial)
  
  
    const addCount = (num) =>{
        setCount(count + num)
       }    

return (
    <>
    <div className="container itemCount">
        <div className='row'>
            <button
            type="button" className="col-1 btn btn-dark"
                onClick={()=>{ addCount(-1) }}
                disabled={ count=== 1 ? true:false }
            >-</button>
            <div className='d-inline text-center'>{count}</div>
            <button
            type="button" className="col-1 btn btn-dark"
                onClick={()=>{ addCount(1) }}
                disabled={ count=== stock ? true:false }
            >+</button>
            <div className='row'>
            <button 
            type="button" className="col-4 btn btn-dark btn-añadirAlCarrito"
            onClick={()=>{onAdd(count, producto); handleInputType();}}>Añadir al carrito</button>
        </div>
        </div>
    </div>
    </>
)

}

export default ItemCount
