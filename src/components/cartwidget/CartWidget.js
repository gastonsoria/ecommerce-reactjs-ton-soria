import { useContext } from 'react'
import CartContext from '../../context/CartContext'
import bolsadecompras from './../../assets/images/eco-bag.png'
import { Link } from 'react-router-dom'


function CartWidget() {

  const cartContext = useContext(CartContext)
  // const { cart } = cartContext

  return (
    <>
    <Link to='/cart' >
    <img className="" src={bolsadecompras} alt="bolsa de compras ecológica"/>
    {/* <span>{ cart.length }</span> */}
    </Link>  
    </>
  )
}

export default CartWidget